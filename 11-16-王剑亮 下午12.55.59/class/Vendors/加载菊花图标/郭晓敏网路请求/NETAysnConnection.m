//
//  NETAysnConnection.m
//  CookMenu
//
//  Created by 郭晓敏 on 14-7-21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "NETAysnConnection.h"
@implementation NETAysnConnection
{
    // 申明实例变量block，用来保存外面传来的的参数 block 代码块，以便系统的代理方法使用
    DidReceiveResponseBlocks    _didReceiveResponse;
    
    DidReceiveDataBlocks            _didReceiveData;
    
    DidFinishLoadingBlocks        _didFinishLoading;
    
    DidFailReceiveDataBlocks     _didFailReceiveData;
    
}

#pragma mark 实现初始化方法
//  指定的初始化方法
-(instancetype)initWithRequest:(NSURLRequest *)request
{
    self = [super initWithRequest:request delegate:self];
    if (self) {
        [self start];
    }
    return self;
}

//2、 便利构造器--通过requestURL快速创建一个 GET请求的异步连接
+(instancetype)asynConnectionWithGetRequest:(NSString *)requestString
{
    NSURL *url = [NSURL URLWithString:requestString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NETAysnConnection *connection = [[NETAysnConnection alloc] initWithRequest:request];
    return connection;
    
}
// 3、便利构造器--通过 requestURL 快速创建一个 POST 请求的异步连接（其中 body 可以为空）
+(instancetype)asynCOnnectionwithPOSTRequest:(NSString *)requestString
                                        body:(NSString *)bodyString
{
    NSURL *url = [NSURL URLWithString:requestString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    if (bodyString && ![bodyString isEqualToString:@""]) {
        NSData *bodyData = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:bodyData];
    }
    NETAysnConnection *connection = [[NETAysnConnection alloc] initWithRequest:request];
    return connection;
}

#pragma mark --用 block回调 NSURLConnectionDelegate 和 NSURLConnectionDataDelegate 方法
//1.  代理方法--已经收到响应
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (_didReceiveResponse) {
        _didReceiveResponse(response);
    }
}

//  回调方法block--收到响应
-(void)didReceiveResponseUsingBlock:(DidReceiveResponseBlocks)didReceiveResponse
{
    _didReceiveResponse = [didReceiveResponse copy];
}



//2.  代理方法--已经收到数据
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (_didReceiveData) {
        _didReceiveData(data);
    }
}

//  回调方法block---已经收到数据
-(void)didReceiveDataUsingBlock:(DidReceiveDataBlocks)didReceiveData
{
    _didReceiveData = [didReceiveData copy];
}



//3.  代理方法--数据加载完成
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (_didFinishLoading) {
        _didFinishLoading();
    }
}
//  回调方法 block---数据已经加载完成
-(void)didFinishLoadingUsingBlock:(DidFinishLoadingBlocks)didFinishLoading
{
    _didFinishLoading = [didFinishLoading copy];
}

//4.  代理方法---链接失败
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (_didFailReceiveData) {
        _didFailReceiveData(error);
    }
}

//  回调方法block--链接失败
-(void)didFailedUsingBlock:(DidFailReceiveDataBlocks)didFailReceiveData
{
    _didFailReceiveData = [didFailReceiveData copy];
}

@end
