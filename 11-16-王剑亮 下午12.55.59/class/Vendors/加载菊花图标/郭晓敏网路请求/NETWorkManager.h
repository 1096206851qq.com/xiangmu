//
//  NETWorkManager.h
//  CookMenu
//
//  Created by 郭晓敏 on 14-7-22.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

// 定义获取到数据的 block
typedef void (^RequestHomePageDataBlock)(NSDictionary *dataDic);
// 根据菜名称获取数据
typedef void (^requestWithMenueNameBlock)(NSDictionary *dataDic);
//  根据菜类型（比如、人群）获取菜单列表
typedef void (^requestWithCookTypeIDBlock)(NSDictionary *dataDic);

@interface NETWorkManager : NSObject
//  创建单例
+(instancetype)sharedInstance;
//  获取首页数据
-(void)requestHomePageDataWithCompletion:(RequestHomePageDataBlock)completion;
//  根据菜单名字获取数据
-(void)requestWithMenueName:(NSString *)menueName
             withStartInsex:(NSInteger)startIndex
             withCompletion:(requestWithMenueNameBlock)completion;
//  根据菜类型（比如、人群）获取菜单列表
-(void)requestDataWithCookTypeID:(NSString *)cookTypeID
                    withCompletion:(requestWithCookTypeIDBlock)completion;
@end
