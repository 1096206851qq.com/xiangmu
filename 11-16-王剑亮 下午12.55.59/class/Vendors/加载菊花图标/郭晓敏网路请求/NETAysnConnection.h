//
//  NETAysnConnection.h
//  CookMenu
//
//  Created by 郭晓敏 on 14-7-21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>
// 定义需要回调的 block方便后面的使用
typedef void (^DidReceiveResponseBlocks)(NSURLResponse * response);

typedef void(^DidReceiveDataBlocks) (NSData * data);

typedef void(^DidFinishLoadingBlocks) (void);

typedef void(^DidFailReceiveDataBlocks) (NSError *error);

@interface NETAysnConnection : NSURLConnection<NSURLConnectionDataDelegate, NSURLConnectionDelegate>

#pragma 初始化方法
// 1、指定初始化方法
-(instancetype)initWithRequest:(NSURLRequest *)request;

//2、 便利构造器--通过requestURL快速创建一个 GET请求的异步连接
+(instancetype)asynConnectionWithGetRequest:(NSString *)requestString;

// 3、便利构造器--通过 requestURL 快速创建一个 POST 请求的异步连接（其中 body 可以为空）
+(instancetype)asynCOnnectionwithPOSTRequest:(NSString *)requestString
                                        body:(NSString *)bodyString;

#pragma mark 回调方法，blocks 回调方法。。通过外面传来的 block 实现外面对获取的到的数据的处理
//  回调方法--已经收到请求
-(void)didReceiveResponseUsingBlock:(DidReceiveResponseBlocks)didReceiveResponse;

//  回调方法--已经收到数据
-(void)didReceiveDataUsingBlock:(DidReceiveDataBlocks)didReceiveData;

//  回调方法--数据获取完成
-(void)didFinishLoadingUsingBlock:(DidFinishLoadingBlocks)didFinishLoading;

//  回调方法--数据获取失败
-(void)didFailedUsingBlock:(DidFailReceiveDataBlocks)didFailReceiveData;

@end
