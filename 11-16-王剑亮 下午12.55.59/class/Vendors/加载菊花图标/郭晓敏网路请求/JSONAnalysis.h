//
//  JSONAnalysis.h
//  CookMenu
//
//  Created by 郭晓敏 on 14-7-21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^DidFailBlocks)(NSError *error);
typedef void (^DidFinishBlocks)(id  jsonObject);
@interface JSONAnalysis : NSObject
#pragma mark 初始化方法
//  1、初始化方法(指定)
-(instancetype)initWithRequest:(NSURLRequest *)request;

//  2、初始化方法POST
-(instancetype)initWithPOSTRequest:(NSString *)request
                          body:(NSString *)body;

//  3、初始化方法 GET
-(instancetype)initWithGETRequest:(NSString *)request;

#pragma mark 回调方法
//  回调方法---数据加载已经结束
-(void)didFinishUsingBlock:(DidFinishBlocks)didFinsh;

//  回调方法---数据加载失败
-(void)didFailUsingBlock:(DidFailBlocks)didFail;
@end
