//
//  NETWorkManager.m
//  CookMenu
//
//  Created by 郭晓敏 on 14-7-22.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "NETWorkManager.h"
#import "JSONAnalysis.h"
static NETWorkManager *netWorkManager = nil;

@interface NETWorkManager ()

@property(nonatomic, strong)NSArray *playArray;

@end

@implementation NETWorkManager
//  创建单例
+(instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (netWorkManager == nil) {
            netWorkManager = [[NETWorkManager alloc] init];
        }
    });
    return netWorkManager;
}
#pragma mark  获取首页数据
-(void)requestHomePageDataWithCompletion:(RequestHomePageDataBlock)completion
{
    self.playArray = [NSArray arrayWithObjects:@"蛋糕",@"菠萝",@"美容",@"猪蹄", @"三文鱼",nil];
    NSInteger select = arc4random() % 5;
    
    RequestHomePageDataBlock requestHomePageDataBlock   = [completion copy];
    NSString * string = [NSString stringWithFormat:@"http://apis.juhe.cn/cook/query?key=a15b28a033d682d96cdc3b87af763ef7&menu=%@&rn=10&pn=0", self.playArray[select]];
    NSString *urlString = [string   stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSLog(@"%@", urlString);
    
    JSONAnalysis *jsonData = [[JSONAnalysis alloc] initWithGETRequest:urlString];
    [jsonData didFinishUsingBlock:^(id jsonObject) {
        if (requestHomePageDataBlock) {
            requestHomePageDataBlock(jsonObject);
        }
    }];
}

//  根据菜单名字获取数据
-(void)requestWithMenueName:(NSString *)menueName withStartInsex:(NSInteger)startIndex withCompletion:(requestWithMenueNameBlock)completion

{
    requestWithMenueNameBlock   requestWithMenueNameblock = [completion copy];
    NSString *urlString = [[NSString stringWithFormat:@"http://apis.juhe.cn/cook/query?key=a15b28a033d682d96cdc3b87af763ef7&menu=%@&rn=10&pn=%ld", menueName, (long)startIndex] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
//    NSLog(@"-------%@", urlString);
    
    JSONAnalysis *jsonData = [[JSONAnalysis alloc] initWithGETRequest:urlString];
    [jsonData didFinishUsingBlock:^(id jsonObject) {
        if (requestWithMenueNameblock) {
            requestWithMenueNameblock(jsonObject);
        }
    }];
    
}

//  根据菜类型（比如、人群）获取菜单列表
-(void)requestDataWithCookTypeID:(NSString *)cookTypeID withCompletion:(requestWithCookTypeIDBlock)completion
{
    requestWithCookTypeIDBlock requestWithCookTypeID = [completion copy];
    NSString *urlString = [[NSString stringWithFormat:@"http://apis.juhe.cn/cook/category?key=a15b28a033d682d96cdc3b87af763ef7&parentid=%@", cookTypeID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    JSONAnalysis *jsonData = [[JSONAnalysis alloc] initWithGETRequest:urlString];
    [jsonData didFinishUsingBlock:^(id jsonObject) {
        if (requestWithCookTypeID) {
            requestWithCookTypeID(jsonObject);
        }
    }];
    
}
@end
