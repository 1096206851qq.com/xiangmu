//
//  AnimationTool.m
//  WhereisYoung
//
//  Created by qianfeng on 14-9-22.
//  Copyright (c) 2015年 陈恒. All rights reserved.
//

#import "AnimationTool.h"


@implementation AnimationTool

+(CATransition *)addAnimationWithAnimationSubType:(NSString *)subType withType:(NSString *)xiaoguo
{
    CATransition *animation=[CATransition animation];
    //立体翻转的效果cube ,rippleEffect,(水波）
    [animation setType:xiaoguo];
    //设置动画方向
    [animation setSubtype:subType];
    //设置动画的动作时长
    [animation setDuration:0.5f];
    //均匀的作用效果
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    return animation;
}

+(CATransition *)addAnimationWithAnimationSubType:(NSString *)subType withType:(NSString *)xiaoguo duration:(CGFloat)duration
{
    CATransition *animation=[CATransition animation];
    //立体翻转的效果cube ,rippleEffect,(水波）
    [animation setType:xiaoguo];
    //设置动画方向
    [animation setSubtype:subType];
    //设置动画的动作时长
    [animation setDuration:duration];
    //均匀的作用效果
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    return animation;
}
@end


