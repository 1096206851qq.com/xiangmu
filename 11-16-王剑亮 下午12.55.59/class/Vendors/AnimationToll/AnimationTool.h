//
//  AnimationTool.h
//  WhereisYoung
//
//  Created by qianfeng on 14-9-22.
//  Copyright (c) 2015年 . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface AnimationTool : NSObject
//动画切换页面的效果
//kCAAnimationCubic迅速透明移动,cube 3D立方体翻页 pageCurl从一个角翻页，pageUnCurl反翻页，rippleEffect水波效果，suckEffect缩放到一个角
//oglFlip中心立体翻转
//（kCATransitionFade淡出，kCATransitionMoveIn覆盖原图，kCATransitionPush推出，kCATransitionReveal卷轴效果
+(CATransition *)addAnimationWithAnimationSubType:(NSString *)subType withType:(NSString *)xiaoguo;
+(CATransition *)addAnimationWithAnimationSubType:(NSString *)subType withType:(NSString *)xiaoguo duration:(CGFloat)duration;
/*****用法示例
////自定义导航动画
//[self.navigationController.view.layer addAnimation:[AnimationTool addAnimationWithAnimationSubType:kCATransitionFromRight withType:@“cube"] forKey:nil];
//
//[self.navigationController pushViewController:second animated:YES];
//
 
 *****************/
@end
