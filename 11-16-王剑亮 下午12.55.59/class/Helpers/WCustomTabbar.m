//
//  WCustomTabbar.m
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "WCustomTabbar.h"
#import "WcustomButton.h"
#import "DebugNSLog.h"
@interface WCustomTabbar ()

@property (nonatomic, strong) NSMutableArray *taBarButtons;
@property (nonatomic, weak) WcustomButton *selectedButton;
@property (nonatomic, weak) UIButton *plusButton;
@property (nonatomic, strong ) UIImageView *buttonView;


@end

@implementation WCustomTabbar


- (NSMutableArray *)taBarButtons{
    if (_taBarButtons == nil) {
        _taBarButtons = [NSMutableArray array];
        
    }
    return _taBarButtons;
}
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        
    }
    return self;
}


- (CGSize)stringWidthWithString:(NSString *)str fontSize:(CGFloat)fontSize contentSize:(CGSize)size
{
    //    第一个参数 代表最大的范围
    //    第二个参数 代表 是否考虑字体、字号
    //    第三个参数 代表使用什么字体、字号
    //    第四个参数 用不到 所以基本为nil
    CGRect stringRect = [str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]} context:nil];
    
    return stringRect.size;
}

- (void)addTabBarButtonWithItem:(UITabBarItem *)item
{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        //先添加一个那啥 背景
        self.buttonView = [[UIImageView alloc] init];
        //self.buttonView.backgroundColor = [UIColor cyanColor];
        self.buttonView.frame = CGRectMake(0, 0, 50, 50);
        self.buttonView.layer.masksToBounds = YES;
        self.buttonView.layer.cornerRadius = 25;
        self.buttonView.image = [UIImage imageNamed:@"cat1"];
        [self addSubview:self.buttonView];
    });
    
    
    
    // 1.创建按钮
    WcustomButton *button = [[WcustomButton alloc]init];
    //button.backgroundColor = [UIColor clearColor];
    [self addSubview:button];
    
    // 添加按钮到数组中
    [self.taBarButtons addObject:button];
    
    
    // 2.设置数据
    button.item = item;
    // 3.监听按钮点击
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchDown];
    // 4.默认选中第0个按钮
    if (self.taBarButtons.count == 1) {
        [self buttonClick:button];
        
       
    }
    
}
/**
 *  监听按钮点击
 */
- (void)buttonClick:(WcustomButton *)button
{
    
    
    // 1.block回传
    if (_block != nil) {
        _block(self.selectedButton.tag,button.tag);
    }else
    {
        MyLog(@"block没有初始化");
    }
    // 2.设置按钮的状态
    self.selectedButton.selected = NO;
    button.selected = YES;
    self.selectedButton = button;
    
    [UIView animateWithDuration:0.5 animations:^
    {
      self.buttonView.center = button.center;
    }];

    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //调整加号按钮的frame
    CGFloat h = self.frame.size.height;
    CGFloat w = self.frame.size.width;
    
    // 按钮的frame数据
    CGFloat buttonH = self.frame.size.height;
    CGFloat buttonW = self.frame.size.width / (self.taBarButtons.count);
    CGFloat buttonY = 0;
    
    for (int index = 0; index<self.taBarButtons.count; index++) {
        // 1.取出按钮
        WcustomButton *button = self.taBarButtons[index];
        
        // 2.设置按钮的frame
        CGFloat buttonX = index * buttonW;
        button.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
   
        // 3.绑定tag
        button.tag = index;
        
        if ((self.taBarButtons.count == 4) && (index == 0)) {
           self.buttonView.center = button.center;
        }
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
