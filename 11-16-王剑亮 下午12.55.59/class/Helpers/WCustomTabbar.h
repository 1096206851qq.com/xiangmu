//
//  WCustomTabbar.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WCustomTabbar : UIView
- (void)addTabBarButtonWithItem:(UITabBarItem *)item;

#pragma mark--- 回传的block用于控制界面的切换

@property (nonatomic, copy) void (^block) (NSInteger from,NSInteger to);





@end
