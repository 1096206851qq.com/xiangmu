//
//  WcustomButton.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WcustomButton : UIButton
@property (nonatomic, strong) UITabBarItem *item;
@end
