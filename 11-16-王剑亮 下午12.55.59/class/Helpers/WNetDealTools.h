//
//  WNetDealTools.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworkReachabilityManager.h"
#import "FMPlayingModel.h"
typedef enum
{
    NewsTypeHeadlines,   //头条
    NewsTypeRecreation,  //娱乐
    NewsTypeSports,      //体育
    NewsTypeFinancial,   //财经
    NewsTypeTechnology,  //科技
    NewsTypeCar,         //汽车
    NewsTypeFashion,     //时尚
    NewsTypeMilitary,    //军事
    NewsTypeGame         //游戏
}NewsType;

@interface WNetDealTools : NSObject
/**
 *  AFNetworkReachabilityStatusUnknown          = -1,  // 未知
 *  AFNetworkReachabilityStatusNotReachable     = 0,   // 无连接
 *  AFNetworkReachabilityStatusReachableViaWWAN = 1,   // 3G
 *  AFNetworkReachabilityStatusReachableViaWiFi = 2,   // 局域网络Wifi
 */
+ (void)checkNetWorkStatus:(void (^)(AFNetworkReachabilityStatus status))block;

#pragma mark--- 输入新闻类型 和你想要的页数每页20个Model block回调 返回新闻类型的 model 数组
+(void)GetNewsModelNewType:(NewsType )type Pages:(NSInteger )Pages block:(void (^)(NSArray *NSArray) )block;


#pragma mark--- 处理cell点击的图片那个数据 给我一个那个model.photosetID
+(void)dealCellModelPhotosetID:(NSString * )str block:(void (^)(NSArray *ModelArray) )block;

//1⃣️
#pragma mark---解析电台数据的方法
+(void)dealRadioModelBlock:(void (^)(NSArray *ModelArray) )block;

#pragma mark---解析电台详情的方法
+(void)dealRadioDetailModelCid:(NSString *)Cid Block:(void (^)(NSArray *))block;


#pragma mark---解析电台每个节目信息数据的方法
+(void)dealRadioPlayListModelTid:(NSString *)Tid pages:(NSInteger)pages Block:(void(^)(NSArray *))block;

#pragma mark-----解析电台播放的数据
+(void)dealRadioPlayModelDocid:(NSString *)Docid Block:(void(^)(NSArray *))block;



#pragma mark---解析视频数据的方法
+(void)GetVideoModelPages:(NSInteger )pages Block:(void (^)(NSArray *ModelArray) )block;



+(void)getFMPlayingDataWithUrl:(NSString*)docid Playblock:(void (^)(FMPlayingModel *model))playBlock;

@end
