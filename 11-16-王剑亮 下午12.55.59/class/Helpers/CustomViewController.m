//
//  CustomViewController.m
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "CustomViewController.h"
#import "WCustomTabbar.h"

@interface CustomViewController ()
@property (nonatomic, weak) WCustomTabbar *customTabBar;
@end

@implementation CustomViewController

//#pragma mark--- 首先移除自己系统带的响应事件的东西 自己定义这个东西以后
//-(void)viewWillAppear:(BOOL)animated
//{
//    for (id child in [self.tabBar subviews]) {
//        
//        if ( [child isKindOfClass:[UIControl class]]) {
//            
//            [child removeFromSuperview];
//        }
//    }
//    
//    self.automaticallyAdjustsScrollViewInsets = YES;
//    
//}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    #pragma mark--- 初始化为第0个界面
    self.selectedIndex = 0;
    #pragma mark--- 初始化tabbar
    [self setupTabbar];
}

/**
 *  初始化tabbar
 */
- (void)setupTabbar
{
    
    #pragma mark--- 初始化一下 自定义的view
    WCustomTabbar *customTabBar = [[WCustomTabbar alloc] init];
    customTabBar.frame = self.tabBar.bounds;
    [self.tabBar addSubview:customTabBar];
    self.customTabBar = customTabBar;
    
    #pragma mark--- 进行block回传
    customTabBar.block = ^void(NSInteger from ,NSInteger to)
    {
        self.selectedIndex = to;
    };
    
}
/**
 *初始化一个自控制器
 */

- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{
    // 1.设置控制器的属性
    childVc.title = title;
    // 设置图标
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    
    childVc.tabBarItem.selectedImage = [ [UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    // 2.包装一个导航控制器
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:childVc];
    
    [self addChildViewController:nav];
    // 3.添加tabbar内部的按钮
    [self.customTabBar addTabBarButtonWithItem:childVc.tabBarItem];
    
}


#pragma mark--- 自定义tabbarcontroller 必须实现四个方法！！！！！！！

-(void)viewWillAppear:(BOOL)animated
{
    
    for (id child in [self.tabBar subviews]) {
        
        if ( [child isKindOfClass:[UIControl class]]) {
            
            [child removeFromSuperview];
        }
    }
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    [self.selectedViewController beginAppearanceTransition: YES animated: animated];
}

-(void) viewDidAppear:(BOOL)animated
{
    [self.selectedViewController endAppearanceTransition];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [self.selectedViewController beginAppearanceTransition: NO animated: animated];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [self.selectedViewController endAppearanceTransition];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
