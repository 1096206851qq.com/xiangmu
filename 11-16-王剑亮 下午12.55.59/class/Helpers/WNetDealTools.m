//
//  WNetDealTools.m
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "WNetDealTools.h"
#import "NewsModels.h"
#import "AFHTTPRequestOperationManager.h"
#import "DebugNSLog.h"
#import "NewsPicture.h"
#import "RadioModel.h"
#import "RadioDetailModel.h"
#import "RadioPlayModel.h"
#import "VideoModel.h"
#import "FMPlayingModel.h"
@interface WNetDealTools ()

@end
static   AFHTTPRequestOperationManager *Mymanager ;

@implementation WNetDealTools


#pragma mark -- get method
#pragma mark--- 获取当前的网络状态
/**
 *  AFNetworkReachabilityStatusUnknown          = -1,  // 未知
 *  AFNetworkReachabilityStatusNotReachable     = 0,   // 无连接
 *  AFNetworkReachabilityStatusReachableViaWWAN = 1,   // 3G
 *  AFNetworkReachabilityStatusReachableViaWiFi = 2,   // 局域网络Wifi
 */
+ (void)checkNetWorkStatus:(void (^)(AFNetworkReachabilityStatus status))block{
    
    // 如果要检测网络状态的变化, 必须要用检测管理器的单例startMoitoring
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // 检测网络连接的单例,网络变化时的回调方法
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
#pragma mark--- 回传回去网络的状态
        block( status );
        
        if(status == AFNetworkReachabilityStatusNotReachable){
            
            NSLog(@"网络连接已断开，请检查您的网络！");
            
            return ;
        }
    }];
    
}
//NewsTypeHeadlines,   //头条
//NewsTypeRecreation,  //娱乐
//NewsTypeSports,      //体育
//NewsTypeFinancial,   //财经
//NewsTypeTechnology,  //科技
//NewsTypeCar,         //汽车
//NewsTypeFashion,     //时尚
//NewsTypeMilitary,    //军事
//NewsTypeGame         //游戏
//1.第一个界面:新闻
//首页新闻接口:1⃣️头条:http://c.3g.163.com/nc/article/headline/T1295501906343/0-20.html
//2⃣️娱乐:http://c.3g.163.com/nc/article/list/T1348648517839/0-20.html
//3⃣️:体育:http://c.3g.163.com/nc/article/list/T1348649079062/0-20.html
//4⃣️财经:http://c.3g.163.com/nc/article/list/T1348648756099/0-20.html
//5⃣️科技:http://c.3g.163.com/nc/article/list/T1348649580692/0-20.html
//6⃣️汽车:http://c.3g.163.com/nc/article/list/T1348654060988/0-20.html
//7⃣️时尚:http://c.3g.163.com/nc/article/list/T1348650593803/0-20.html
//8⃣️军事:http://c.3g.163.com/nc/article/list/T1348648141035/0-20.html
//9⃣️游戏:http://c.3g.163.com/nc/article/list/T1348654151579/0-20.html
//新闻详情界面接口:http://c.3g.163.com/nc/article/%@/full.html
//通过上一界面传来的docIid进行拼接

#pragma mark--- 输入新闻类型 和你想要的页数每页20个Model block回调 返回新闻类型的 model 数组
+(void)GetNewsModelNewType:(NewsType )type Pages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    
    switch (type)
    {
        case NewsTypeHeadlines: [self dealHeadlinesPages:Pages block:block];
            break;
        case NewsTypeRecreation: [self dealRecreationsPages:Pages block:block];
            break;
        case NewsTypeSports: [self dealSportsPages:Pages block:block];
            break;
        case NewsTypeFinancial: [self dealFinancialPages:Pages block:block];
            break;
        case NewsTypeTechnology: [self dealTechnologyPages:Pages block:block];
            break;
        case NewsTypeCar: [self dealCarPages:Pages block:block];
            break;
        case NewsTypeFashion: [self dealFashionPages:Pages block:block];
            break;
        case NewsTypeMilitary: [self dealMilitaryPages:Pages block:block];
            break;
        case NewsTypeGame: [self dealGamePages:Pages block:block];
            break;
            
        default:
            break;
    }
    
}
#pragma mark--- 解析网络下载的字典数据
+(NSArray *)DealNetLoadDic:(NSDictionary *)dic
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    //解析字典变成Model数组
    NSArray *Array = (NSArray *)[dic allValues][0];
    //MyLog(@"%@",Array);
    //解析成model
    for (NSDictionary *dic in Array)
    {
        NewsModels *model = [[NewsModels alloc] init];
        [model setValuesForKeysWithDictionary:dic];
#pragma mark--- 添加到数据model数组
        [arr addObject:model];
    }
    
    return arr;
}
#pragma mark--- 处理头条的解析
+(void)dealHeadlinesPages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    
    
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/headline/T1295501906343/%ld-%ld.html",Pages*20,(Pages+1)*20];
    Mymanager = [AFHTTPRequestOperationManager manager];
    
    Mymanager.responseSerializer.acceptableContentTypes= [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {     //NSLog(@"Error: %@", error);
     }];
}
#pragma mark--- 处理娱乐的解析
+(void)dealRecreationsPages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/T1348648517839/%ld-%ld.html",Pages*20,(Pages+1)*20];
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {    // NSLog(@"Error: %@", error);
     }];
}
#pragma mark--- 处理体育的解析
+(void)dealSportsPages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/T1348649079062/%ld-%ld.html",Pages*20,(Pages+1)*20];
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        // MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {     //NSLog(@"Error: %@", error);
     }];
}
#pragma mark--- 处理财经的解析
+(void)dealFinancialPages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/T1348648756099/%ld-%ld.html",Pages*20,(Pages+1)*20];
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {     //NSLog(@"Error: %@", error);
     }];
}
#pragma mark--- 处理科技的解析
+(void)dealTechnologyPages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/T1348649580692/%ld-%ld.html",Pages*20,(Pages+1)*20];
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {    // NSLog(@"Error: %@", error);
     }];
}
#pragma mark--- 处理汽车的解析
+(void)dealCarPages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/T1348654060988/%ld-%ld.html",Pages*20,(Pages+1)*20];
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {     //NSLog(@"Error: %@", error);
     }];
}
#pragma mark--- 处理时尚的解析
+(void)dealFashionPages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/T1348650593803/%ld-%ld.html",Pages*20,(Pages+1)*20];
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {    // NSLog(@"Error: %@", error);
     }];
}
#pragma mark--- 处理军事的解析
+(void)dealMilitaryPages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/T1348648141035/%ld-%ld.html",Pages*20,(Pages+1)*20];
    
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {     //NSLog(@"Error: %@", error);
     }];
}

#pragma mark--- 处理游戏的解析
+(void)dealGamePages:(NSInteger )Pages block:(void (^)(NSArray *ModelArray) )block
{
    NSString *string = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/T1348654151579/%ld-%ld.html",Pages*20,(Pages+1)*20];
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools DealNetLoadDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {    // NSLog(@"Error: %@", error);
     }];
}


#pragma mark--- 处理cell点击的图片那个数据 给我一个那个model.photosetID
+(void)dealCellModelPhotosetID:(NSString * )str block:(void (^)(NSArray *ModelArray) )block
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
#pragma mark--- 分析数据
    //单独是图片的数据
    NSArray * arraystr = [str componentsSeparatedByString:@"|"];
    NSString * strr = [arraystr.firstObject substringFromIndex:4];
#pragma mark--- 拼接数据信息
    strr = [NSString stringWithFormat:@"%@/%@",strr,arraystr.lastObject];
    NSString *string = [NSString stringWithFormat:@"http://c.m.163.com/photo/api/set/%@.json",strr];
   // NSLog(@"%@",string);
    
    
    [Mymanager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        // MyLog(@"JSON: %@", responseObject);
         
         NSDictionary *dic = responseObject;
         
         NSArray *arr = [dic objectForKey:@"photos"];
         
         //解析model
         for (NSDictionary *dic1 in arr) {
             NewsPicture *model = [[NewsPicture alloc] init];
             
             NSString *imageStr = [dic1 objectForKey:@"imgurl"];
             
             [model setValue:imageStr forKey:@"imageUrl"];
             [array addObject:model];
         }
         
         block(array);
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {     //NSLog(@"Error: %@", error);
     }];
}

//3⃣️
#pragma mark---解析电台数据的方法
+(void)dealRadioModelBlock:(void (^)(NSArray *ModelArray) )block
{
    //网址
    NSString *urlStr = [NSString stringWithFormat:@"http://c.3g.163.com/nc/topicset/ios/radio/index.html"];
    
    [Mymanager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools dealRadioDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {    // NSLog(@"Error: %@", error);
     }];
    
}

//2⃣️
+ (NSArray *)dealRadioDic:(NSDictionary *)dic
{
    NSMutableArray *radioArray = [NSMutableArray array];

    NSArray *arr1 = dic[@"cList"];
    
    for (NSDictionary *dic1 in arr1)
    {
        NSString *cid = dic1[@"cid"];
        NSString *cname = dic1[@"cname"];
        NSArray *array2 = dic1[@"tList"];
        
        NSMutableArray *radioArray1 = [NSMutableArray array];
        for (NSDictionary *dic2 in array2)
        {
            RadioModel *model = [[RadioModel alloc] init];
            //设置 cid 和 name
            [model setValue:cid  forKey:@"cid"];
            [model setValue:cname  forKey:@"cname"];
            //设置其他信息
            [model setValuesForKeysWithDictionary:dic2];
            //解析radio中的图片和title(标题描述)
            NSDictionary *dic3 = dic2[@"radio"];
            NSString *imgsrc = dic3[@"imgsrc"];
            NSString *title = dic3[@"title"];
            [model setValue:imgsrc forKey:@"imgsrc"];
            [model setValue:title forKey:@"title"];
            [radioArray1 addObject:model];
            
        }
        
        [radioArray addObject:radioArray1];
    }
    
    return radioArray;
}

#pragma mark 解析电台详情的
+(NSArray *)dealRadioDetailDic:(NSDictionary *)dic
{
    NSMutableArray *radioDetailArray = [NSMutableArray array];
    
    NSArray *arr1 = dic[@"tList"];
    
    for (NSDictionary *dic1 in arr1)
    {
        NSDictionary *dic2 = dic1[@"radio"];
        NSString *tid = dic1[@"tid"];
        RadioDetailModel *model = [[RadioDetailModel alloc] init];
        [model setValuesForKeysWithDictionary:dic2];
        [model setValue:tid forKey:@"tid"];
        [radioDetailArray addObject:model];
      
    }
    
    return radioDetailArray;
  
}

#pragma mark---解析电台详情数据的方法
+(void)dealRadioDetailModelCid:(NSString *)Cid Block:(void (^)(NSArray *))block
{
        NSString *urlStr = [NSString stringWithFormat:@"http://c.3g.163.com/nc/topicset/ios/radio/%@/0-20.html",Cid];
    
    
        [Mymanager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             //MyLog(@"JSON: %@", responseObject);
             #pragma mark--- 解析成model数组回传回去
             block([WNetDealTools dealRadioDetailDic:responseObject]);
         } failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {     NSLog(@"Error: %@", error);
         }];
}





#pragma mark---解析电台每个节目信息数据的方法
+(NSArray *)dealRadioPlayDic:(NSDictionary *)dic
{
    NSMutableArray *radioPlayArray = [NSMutableArray array];
   
    NSArray *arr1 = [dic allValues][0];
    for (NSDictionary *dic1 in arr1) {
        RadioPlayModel *model = [[RadioPlayModel alloc] init];
        [model setValuesForKeysWithDictionary:dic1];
        [radioPlayArray addObject:model];
    }
    return radioPlayArray;
}

#pragma mark---解析电台每个节目信息数据的方法
+(void)dealRadioPlayListModelTid:(NSString *)Tid pages:(NSInteger)pages Block:(void(^)(NSArray *))block
{
    NSString *urlStr = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/list/%@/%ld-%ld.html",Tid,pages*20,(pages+1)*20];
    
   
    [Mymanager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //MyLog(@"JSON: %@", responseObject);
#pragma mark--- 解析成model数组回传回去
         block([WNetDealTools dealRadioPlayDic:responseObject]);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {     NSLog(@"Error: %@", error);
     }];

}

//#pragma mark--------解析电台播放的数据
//+(NSArray *)dealRadioPlayVCDic:(NSDictionary *)dic
//{
//    return nil;
//}
//
//#pragma mark--------解析电台播放的数据
//+(void)dealRadioPlayModelDocid:(NSString *)Docid Block:(void(^)(NSArray *))block
//{
//    NSString *urlStr = [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/%@/full.html",Docid];
//    
//    
//    [Mymanager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         //MyLog(@"JSON: %@", responseObject);
//#pragma mark--- 解析成model数组回传回去
//         block([WNetDealTools dealRadioPlayVCDic:responseObject]);
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {     NSLog(@"Error: %@", error);
//     }];
//    
//}


#pragma mark---解析视频数据的方法
+(void)GetVideoModelPages:(NSInteger )pages Block:(void (^)(NSArray *ModelArray) )block
{
        //网址
        NSString *urlStr = [NSString stringWithFormat:@"http://c.3g.163.com/nc/video/home/%ld-%ld.html",pages*20,(pages+1)*20];

        [Mymanager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             //MyLog(@"JSON: %@", responseObject);
    #pragma mark--- 解析成model数组回传回去
             block([WNetDealTools dealVideoDic:responseObject]);
         } failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {     //NSLog(@"Error: %@", error);
         }];
        
}

+ (NSArray *)dealVideoDic:(NSDictionary *)dic
{
    NSMutableArray *radioArray = [NSMutableArray array];

    NSArray *arr1 = dic[@"videoList"];

    for (NSDictionary *dic1 in arr1) {

        VideoModel *model = [[VideoModel alloc] init];

        [model setValuesForKeysWithDictionary:dic1];

        [radioArray addObject:model];
    }

    return radioArray;
}

/*电台播放详情*/
#define FM_PLAY_URL(docid) [NSString stringWithFormat:@"http://c.3g.163.com/nc/article/%@/full.html",docid]
//解析播放数据-获取正在播放数据
+(void)getFMPlayingDataWithUrl:(NSString*)docid Playblock:(void (^)(FMPlayingModel *model))playBlock
{
    NSURL * url = [NSURL URLWithString:FM_PLAY_URL(docid)];
    dispatch_queue_t queue = dispatch_queue_create("com.mengliang.zhixun.playing", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        NSData * data = [NSData dataWithContentsOfURL:url];
        if (!data) {
            return;
        }
        NSLog(@"url ===========------ %@",url);
        NSDictionary * h_dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSDictionary * b_dic = h_dic[docid];
        NSDictionary *dic2  = b_dic[@"video"][0];
        
        FMPlayingModel * model = [[FMPlayingModel alloc]init];
        
        
        
        [model setValuesForKeysWithDictionary:dic2];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(playBlock)
            playBlock(model);
        });
    });
}





@end
