//
//  CustomViewController.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomViewController : UITabBarController
- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName;
@end
