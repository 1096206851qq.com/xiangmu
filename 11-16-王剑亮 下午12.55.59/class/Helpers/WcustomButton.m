//
//  WcustomButton.m
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "WcustomButton.h"
#import "UIImage+LYY.h"
//图标的比例
#define LYYTabBarButtonImageRatio 0.6
// 按钮的默认文字颜色
#define  LYYTabBarButtonTitleColor [UIColor whiteColor]
// 按钮的选中文字颜色
#define LYYColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define  LYYTabBarButtonTitleSelectedColor LYYColor(214, 36, 111)

@interface WcustomButton ()
@property (nonatomic, strong) UIButton *badgeValueLable;

@end

@implementation WcustomButton

- (id)init
{
    self = [super init];
    if (self) {
        // 图标居中
        self.imageView.contentMode = UIViewContentModeCenter;
        // 文字居中
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        // 字体大小
        self.titleLabel.font = [UIFont systemFontOfSize:12];
        // 文字颜色
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self setTitleColor:LYYTabBarButtonTitleSelectedColor forState:UIControlStateSelected];
        
        #pragma mark--- 设置好badgeValue的提醒
        _badgeValueLable = [UIButton buttonWithType:UIButtonTypeCustom];
         [_badgeValueLable setBackgroundColor:[UIColor redColor]];
        _badgeValueLable.titleLabel.font = [UIFont systemFontOfSize:12];
        [_badgeValueLable.layer setMasksToBounds:YES];
        [_badgeValueLable.layer setCornerRadius:10.0];
        [self addSubview:_badgeValueLable];
        
        
        
    }
    return self;
}

// 重写去掉高亮状态
- (void)setHighlighted:(BOOL)highlighted {}

// 内部图片的frame
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.height * LYYTabBarButtonImageRatio;
    return CGRectMake(0, 0, imageW, imageH);
}

// 内部文字的frame
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleY = contentRect.size.height * LYYTabBarButtonImageRatio;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    return CGRectMake(0, titleY, titleW, titleH);
}

// 设置item
- (void)setItem:(UITabBarItem *)item
{
    _item = item;
    //设置文字
    [item addObserver:self forKeyPath:@"badgeValue" options:0 context:nil];
    [item addObserver:self forKeyPath:@"title" options:0 context:nil];
    [item addObserver:self forKeyPath:@"image" options:0 context:nil];
    [item addObserver:self forKeyPath:@"selectedImage" options:0 context:nil];
    [self observeValueForKeyPath:nil ofObject:nil change:nil context:nil];
    
}
- (void)dealloc
{
    [self.item removeObserver:self forKeyPath:@"badgeValue"];
    [self.item removeObserver:self forKeyPath:@"title"];
    [self.item removeObserver:self forKeyPath:@"image"];
    [self.item removeObserver:self forKeyPath:@"selectedImage"];
}
/**
 *  监听到某个对象的属性改变了,就会调用
 *
 *  @param keyPath 属性名
 *  @param object  哪个对象的属性被改变
 *  @param change  属性发生的改变
 */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    // 设置文字
    [self setTitle:self.item.title forState:UIControlStateSelected];
    [self setTitle:self.item.title forState:UIControlStateNormal];
    
    // 设置图片
    [self setImage:self.item.image forState:UIControlStateNormal];
    [self setImage:self.item.selectedImage forState:UIControlStateSelected];
    
    //设置提醒数字 这里开始的时候要创建一个 badgeValue
    if(self.item.badgeValue)
    {
        _badgeValueLable.hidden = NO;
        
        if (self.item.badgeValue.integerValue > 99)
        {
            _badgeValueLable.frame = CGRectMake(45, 0,30, 20);
            [_badgeValueLable setTitle:@"99+" forState: UIControlStateNormal]; 
        }
        else
        {
            [_badgeValueLable setTitle:self.item.badgeValue forState: UIControlStateNormal];
            _badgeValueLable.frame = CGRectMake(45, 0,20, 20);
        }
    
    }
    else
    {
      _badgeValueLable.hidden = YES;
    
    }


    
    
    
}






/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
