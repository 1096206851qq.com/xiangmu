//
//  DebugNSLog.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#ifndef DebugNSLog_h
#define DebugNSLog_h

#pragma mark--- 定义了调试模式 上线的时候打开关闭调试模式

#define DeBug

#ifdef DeBug

#define MyLog(...)  NSLog(__VA_ARGS__)

#else

#define MyLog(...)  { }

#endif

#endif /* DebugNSLog_h */
