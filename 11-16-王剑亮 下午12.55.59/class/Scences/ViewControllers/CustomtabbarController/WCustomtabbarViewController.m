//
//  WCustomtabbarViewController.m
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "WCustomtabbarViewController.h"
#import "NewsTableViewController.h"
#import "RadioViewController.h"
#import "VideoTableViewController.h"
#import "UserTableViewController.h"

@interface WCustomtabbarViewController ()

@end

@implementation WCustomtabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    #pragma mark--- 添加自己的tabbarviewcontroller的控制器 带navgationController
    [self setupAllChildViewControllers];
    
    
}


/*
 *初始化所有的自控制器
 */

- (void)setupAllChildViewControllers{
 
//    message.tabBarItem.badgeValue = @"3";
////    home.tabBarItem.badgeValue = @"10";
////    discover.tabBarItem.badgeValue = nil;
////    me.tabBarItem.badgeValue = @"153";
    
    NewsTableViewController *newsVC = [[NewsTableViewController alloc] init];
    [self setupChildViewController:newsVC title:@"新闻" imageName:@"iconfont-shenghuojiaofe" selectedImageName:@"iconfont-iconfontshenghuojiaofei"];
    RadioViewController *radioVC = [[RadioViewController alloc] init];
    [self setupChildViewController:radioVC title:@"电台" imageName:@"iconfont-iconmobantuijia" selectedImageName:@"iconfont-iconmobantuijianfanxiang"];
    VideoTableViewController *videoVC = [[VideoTableViewController alloc] init];
    [self setupChildViewController:videoVC title:@"视频" imageName:@"iconfont-faxia" selectedImageName:@"iconfont-faxian"];
    UserTableViewController *userVC = [[UserTableViewController alloc] init];
    [self setupChildViewController:userVC title:@"个人中心" imageName:@"iconfont-wod" selectedImageName:@"iconfont-wode"];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
