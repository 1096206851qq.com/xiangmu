//
//  AboutUserViewController.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/19.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "AboutUserViewController.h"

@interface AboutUserViewController ()
@property (nonatomic, strong) UILabel *lable;
@end

@implementation AboutUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.translucent = NO;
    self.lable = [[UILabel alloc] initWithFrame:self.view.frame];
    self.lable.backgroundColor = [UIColor whiteColor];
    self.lable.text = @"本app所有内容，包括文字、图片、音频、视频、软件、程序、以及版式设计等均在网上搜集。\n\t访问者可将本app提供的内容或服务用于个人学习、研究或欣赏，以及其他非商业性或非盈利性用途，但同时应遵守著作权法及其他相关法律的规定，不得侵犯本app及相关权利人的合法权利。除此以外，将本app任何内容或服务用于其他用途时，须征得本app及相关权利人的书面许可，并支付报酬。\n\t本app内容原作者如不愿意在本app刊登内容，请及时通知本app，予以删除。\n\t地址：北京市海淀区清河毛纺路路南甲36号金五星商业大厦五层\n\t电话：15333815726\n\t电子邮箱：1096206851@qq.com";
    self.lable.font = [UIFont fontWithName:@"STHeiti-Medium.ttc" size:64];
    
    self.lable.numberOfLines = 0;
    [self.lable sizeToFit];
    [self.view addSubview:self.lable];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
