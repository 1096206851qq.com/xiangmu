//
//  RadioDetailViewController.h
//  LWB_Project
//
//  Created by lanou3g on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioModel.h"
@interface RadioDetailViewController : UIViewController

@property (nonatomic , strong) RadioModel *model;

+(instancetype)shareRadioDetailViewController;
@end
