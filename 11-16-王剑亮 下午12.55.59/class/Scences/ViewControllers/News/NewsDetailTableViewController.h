//
//  NewsDetailTableViewController.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/13.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModels.h"
@interface NewsDetailTableViewController : UIViewController

@property (nonatomic, strong) NewsModels *model;


@end
