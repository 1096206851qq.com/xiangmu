//
//  RadioPlayViewController.h
//  LWB_Project
//
//  Created by lanou3g on 15/11/17.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioModel.h"

@interface RadioPlayViewController : UIViewController


@property (nonatomic, strong) RadioModel *model;

@property (nonatomic, strong) NSString *tidStr;

+(instancetype)shareRadioPlayViewController;

@end
