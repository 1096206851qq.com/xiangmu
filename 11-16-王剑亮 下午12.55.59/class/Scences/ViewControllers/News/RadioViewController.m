//
//  RadioViewController.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/11.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "RadioViewController.h"
#import "RadioDetailViewController.h"
#import "WNetDealTools.h"
#import "RadioModel.h"
#import "RadioTableViewCell.h"
#import "RadioDetailViewController.h"
#import "RadioPlayViewController.h"
#import "UIImageView+WebCache.h"
#import "DataBasehandle.h"
#define KDataBasehandle [DataBasehandle sharedDataBasehandle]

@interface RadioViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *radioTableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) RadioModel *headModel;
@end

@implementation RadioViewController

static RadioViewController *share;

+(instancetype)shareRadioViewController
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        share = [[RadioViewController alloc] init];
    });
    return share;
}

-(void)viewWillAppear:(BOOL)animated
{

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.translucent = YES;
    [self loadRadioTableView];
    [self loadHeadImageView];
    
    NSString *NetStates = [[NSUserDefaults standardUserDefaults] objectForKey:@"NETStates"];
    if ([NetStates isEqualToString:@"YES"])
    {
        //4⃣️
        self.dataArray = [NSMutableArray array];
        __block RadioViewController *vc = self;
        [WNetDealTools dealRadioModelBlock:^(NSArray *ModelArray) {
            [vc.dataArray addObjectsFromArray:ModelArray];
            [vc.radioTableView reloadData];
            
            //存储数据 ModelArray是个两层的数组
            int i = 0;
            for (NSArray *arr in ModelArray)
            {
                [KDataBasehandle createtableName:[NSString stringWithFormat:@"RadioModel%d",i++] Modelclass:[RadioModel class]];
                for (RadioModel *model in arr) {
                    
                    [KDataBasehandle insertIntoTableModel:model];
                }
                
            }
        }];
    
    }
    else
    {
     #pragma mark---读出来数据库中的东西刷新  之后 提示没有网
        self.dataArray = [NSMutableArray array];
        //存储数据 ModelArray是个两层的数组
        for (int i = 0; i < 8; i++) {
            [KDataBasehandle createtableName:[NSString stringWithFormat:@"RadioModel%d",i++] Modelclass:[RadioModel class]];
            NSArray *arr = [KDataBasehandle selectAllModel] ;
            [self.dataArray addObject:arr];
        }
        
        [self.radioTableView reloadData];
        [self checkNetalerView];
    }


   
}


#pragma mark------创建tableView
- (void)loadRadioTableView
{
    self.radioTableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStylePlain)];
    self.radioTableView.dataSource = self;
    self.radioTableView.delegate = self;
    [self.view addSubview:self.radioTableView];
    [self.radioTableView registerNib:[UINib nibWithNibName:@"RadioTableViewCell" bundle:nil] forCellReuseIdentifier:@"qwer"];
}
-(void)loadHeadImageView
{
    
    self.headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height / 4)];
    self.headImageView.image = [UIImage imageNamed:@"u=2048236986,3315881605&fm=11&gp=0.jpg"];
    self.radioTableView.tableHeaderView = self.headImageView;
    
    
    self.headImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enterPlayVC)];
    [self.headImageView addGestureRecognizer:tap];
    
}

- (void)enterPlayVC
{
    RadioPlayViewController *radioPlayVC = [RadioPlayViewController shareRadioPlayViewController];
    
    [self.navigationController pushViewController:radioPlayVC animated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat w = [UIScreen mainScreen].bounds.size.width;
    return (100 * w/320 +145) * 7/6 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"qwer" forIndexPath:indexPath];
    NSArray *array = self.dataArray[indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.Array = array;
#pragma mark---点击进入按钮可以进入下个页面并将cid传过去
    __weak RadioViewController *radio = self;
    cell.enterBlock = ^(RadioModel *model)
    {
        
        NSString *NetStates = [[NSUserDefaults standardUserDefaults] objectForKey:@"NETStates"];
        if ([NetStates isEqualToString:@"YES"])
        {
            RadioDetailViewController *radioDetailVC = [[RadioDetailViewController alloc]init];
            radioDetailVC.model = model;
            [radio.navigationController pushViewController:radioDetailVC animated:YES];
        }else
        {
          [self checkNetalerView];
        }

        
    };
    cell.enterBlockOne = ^(RadioModel *Model){
        
        NSString *NetStates = [[NSUserDefaults standardUserDefaults] objectForKey:@"NETStates"];
        if ([NetStates isEqualToString:@"YES"])
        {
            RadioPlayViewController *radioPlayVC = [RadioPlayViewController shareRadioPlayViewController];
            radioPlayVC.model = Model;
            [radio.navigationController pushViewController:radioPlayVC animated:YES];
        }else
        {
            [self checkNetalerView];
        
        }

       
    };
    return cell;
}
-(void)checkNetalerView
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请检查网络连接" preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确认" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    RadioDetailViewController *radioDetailVC = [[RadioDetailViewController alloc] initWithNibName:@"RadioDetailViewController" bundle:nil];
//    
//    [self.navigationController pushViewController:radioDetailVC animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
