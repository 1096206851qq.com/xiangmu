//
//  RadioDetailViewController.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "RadioDetailViewController.h"
#import "RadioDetailTableViewCell.h"
#import "RadioPlayViewController.h"
#import "WNetDealTools.h"
@interface RadioDetailViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *radioTableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation RadioDetailViewController
static RadioDetailViewController *share;
+(instancetype)shareRadioDetailViewController
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        share = [[RadioDetailViewController alloc] init];
    });
    return share;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.dataArray = [NSMutableArray array];
    __block RadioDetailViewController *detailVC = self;
    [WNetDealTools dealRadioDetailModelCid:_model.cid Block:^(NSArray *ModelArray) {
        
        [detailVC.dataArray addObjectsFromArray:ModelArray];
        [detailVC.radioTableView reloadData];
        
    }];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTableView];
    //把图片设成圆形
//    [self.view layoutIfNeeded];//使用storyboard和xib的时候用这种方法
    self.navigationController.navigationBar.translucent = NO;
//    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.picImageView.layer.masksToBounds = YES;
//    self.picImageView.layer.cornerRadius = 90;
//    self.picImageView.layer.cornerRadius = CGRectGetHeight(_picImageView.frame)/2;
    //self.picImageView.layer.cornerRadius = (self.picImageView.frame.size.width)/2;
    
   
}

- (void)loadTableView
{
    self.radioTableView.dataSource = self;
    self.radioTableView.delegate = self;
    
    [self.radioTableView registerNib:[UINib nibWithNibName:@"RadioDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"u=1961&gp=0.jpg"]];
    
    RadioDetailModel *model = self.dataArray[indexPath.row];
    
    
    [cell setModel:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioPlayViewController *radioPlayVC = [[RadioPlayViewController alloc] init];
    
    RadioDetailModel *model = self.dataArray[indexPath.row];
    
    RadioModel *model1 = [[RadioModel alloc] init];
    model1.tid = model.tid;
    model1.imgsrc = model.imgsrc;
    radioPlayVC.model= model1;
    [self.navigationController pushViewController:radioPlayVC animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
