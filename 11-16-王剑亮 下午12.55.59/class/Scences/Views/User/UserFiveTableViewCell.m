//
//  UserSevenTableViewCell.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/14.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "UserFiveTableViewCell.h"
#import "DebugNSLog.h"
#import "AppDelegate.h"
@interface UserSevenTableViewCell ()
@property (strong, nonatomic) IBOutlet UISwitch *myswitch;

@end

@implementation UserSevenTableViewCell
-(void)addCellinit
{
    [self.myswitch addTarget:self action:@selector(changeSwitchAction:) forControlEvents:(UIControlEventValueChanged)];

}

- (void)changeSwitchAction:(UISwitch *)sender
{
    MyLog(@"夜间模式切换");
    
    
   AppDelegate *app  = [UIApplication sharedApplication].delegate;

    if (sender.isOn == YES) {
        app.window.alpha = 0.5;
        
    }else{
        app.window.alpha = 1;
    }
}
- (void)awakeFromNib {
    
    
    [self addCellinit];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
