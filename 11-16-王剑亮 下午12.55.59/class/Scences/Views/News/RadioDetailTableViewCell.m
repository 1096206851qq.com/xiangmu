//
//  RadioDetailTableViewCell.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/17.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "RadioDetailTableViewCell.h"
#import "UIImageView+WebCache.h"
@interface RadioDetailTableViewCell ()
@property (strong, nonatomic) IBOutlet UIImageView *radioImageView;

@property (strong, nonatomic) IBOutlet UILabel *titleLable;

@property (strong, nonatomic) IBOutlet UILabel *nameLable;



@end

@implementation RadioDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];

}




- (void)setModel:(RadioDetailModel *)Model
{
    if ([Model.imgsrc isEqualToString:@""]) {
        self.radioImageView.image =[UIImage imageNamed:@"u=1470722442,2591575796&fm=11&gp=0.jpg"];
    }else{
        [self.radioImageView sd_setImageWithURL:[NSURL URLWithString:Model.imgsrc]];
    }
    
    self.titleLable.text =Model.tname;
    self.nameLable.text = Model.title;

}


- (void)awakeFromNib {
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
