//
//  RadioTableViewCell.h
//  LWB_Project
//
//  Created by lanou3g on 15/11/16.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioModel.h"
//用block吧cid传到下个界面
typedef void(^EnterBlock)(RadioModel *model);
typedef void(^EnterBlock1)(RadioModel *model);

@interface RadioTableViewCell : UITableViewCell

@property (nonatomic, copy) EnterBlock enterBlock;
@property (nonatomic, copy) EnterBlock1 enterBlockOne;

@property (nonatomic,strong) NSArray *Array;

//通过下标设置标题图片
-(void)setupImageForTitleAtindex:(NSInteger)index;

@end
