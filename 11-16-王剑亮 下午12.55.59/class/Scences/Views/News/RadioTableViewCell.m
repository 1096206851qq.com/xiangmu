//
//  RadioTableViewCell.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/16.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "RadioTableViewCell.h"
#import "UIImageView+WebCache.h"
@interface RadioTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *titleImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLable;
@property (strong, nonatomic) IBOutlet UIImageView *oneImageView;
@property (strong, nonatomic) IBOutlet UIImageView *twoImageView;
@property (strong, nonatomic) IBOutlet UIImageView *threeImageView;
@property (strong, nonatomic) IBOutlet UILabel *oneTitleLable;
@property (strong, nonatomic) IBOutlet UILabel *oneNameLable;
@property (strong, nonatomic) IBOutlet UILabel *onePlayCountLable;
@property (strong, nonatomic) IBOutlet UILabel *twoTitleLable;
@property (strong, nonatomic) IBOutlet UILabel *twoNameLable;
@property (strong, nonatomic) IBOutlet UILabel *twoPlayCountLable;
@property (strong, nonatomic) IBOutlet UILabel *threeTitleLable;
@property (strong, nonatomic) IBOutlet UILabel *threeNameLable;
@property (strong, nonatomic) IBOutlet UILabel *threePlayCount;
@property (weak, nonatomic) IBOutlet UIButton *enterRadioDetailButton;
@property (nonatomic, strong) RadioModel *model;

@end
@implementation RadioTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}



-(void)setArray:(NSArray *)Array
{
    
    _Array = Array;

    RadioModel *model1 = Array[0];
    RadioModel *model2 = Array[1];
    RadioModel *model3 = Array[2];
    _model = model1;

    
    self.titleLable.text = model1.cname;
    if ([model1.imgsrc isEqualToString:@""]) {
       //给一个固定的 本地图片!!!!!!
        self.oneImageView.image = [UIImage imageNamed:@"u=1470722442,2591575796&fm=11&gp=0.jpg"];
        
    }else
    {
     [self.oneImageView sd_setImageWithURL:[NSURL URLWithString:model1.imgsrc] placeholderImage:[UIImage imageNamed:@"MenuBackground"] options:(SDWebImageRetryFailed) progress:^(NSInteger receivedSize, NSInteger expectedSize) {
         
     } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
         if (image) {
             self.titleImageView.image = image;
         }
     }];
    }

    [self.twoImageView sd_setImageWithURL:[NSURL URLWithString:model2.imgsrc]];
    [self.threeImageView sd_setImageWithURL:[NSURL URLWithString:model3.imgsrc]];
    self.oneTitleLable.text = model1.tname;
    self.twoTitleLable.text = model2.tname;
    self.threeTitleLable.text = model3.tname;
    
    self.oneNameLable.text = model1.title;
    self.oneNameLable.numberOfLines = 0;
    [self.oneNameLable sizeToFit];
    
    self.twoNameLable.text = model2.title;
    self.twoNameLable.numberOfLines = 0;
    [self.twoNameLable sizeToFit];
    
    self.threeNameLable.text = model3.title;
    self.threeNameLable.numberOfLines = 0;
    [self.threeNameLable sizeToFit];
    
    self.onePlayCountLable.text = [NSString stringWithFormat:@"%@",model1.playCount];
    self.twoPlayCountLable.text = [NSString stringWithFormat:@"%@",model2.playCount];
    self.threePlayCount.text = [NSString stringWithFormat:@"%@",model3.playCount];
}

//点击进入按钮进入每个板块电台分类的是所有种类，需要用block传值
#pragma mark--点击进入按钮进入电台详情
- (IBAction)enterClickButton:(id)sender
{
    
    if (_enterBlock) {
        _enterBlock(_model);
    }

}

#pragma mark---点击第一个图片的button进入播放页面
- (IBAction)enterRadioPlayButton:(id)sender
{
    if (_enterBlockOne) {
        _enterBlockOne(_Array[0]);
    }
}

#pragma mark----点击第二个图片的button
- (IBAction)enterRadioPlayTwoButton:(id)sender
{
    if (_enterBlockOne) {
        _enterBlockOne(_Array[1]);
    }
}

#pragma mark---点击第三个图片的button
- (IBAction)enterRadioPlayThreeButton:(id)sender
{
    if (_enterBlockOne) {
        _enterBlockOne(_Array[2]);
    }
}





- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
