//
//  FirstNewsTableViewCell.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "FirstNewsTableViewCell.h"
#import "UIImageView+WebCache.h"
@interface FirstNewsTableViewCell()

@property (strong, nonatomic) IBOutlet UIImageView *newsImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLable;
@property (strong, nonatomic) IBOutlet UILabel *detailLable;

@end

@implementation FirstNewsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}



-(void)setModel:(NewsModels *)model
{
    self.titleLable.text = model.title;
    self.detailLable.text = model.digest;
    [self.newsImageView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc]];
    
    //设置一下具体的细节
    
    
//    self.detailLable.backgroundColor = [UIColor clearColor];
//    self.titleLable.backgroundColor  = [UIColor clearColor];
    
    self.detailLable.numberOfLines = 0;
    [self.detailLable sizeToFit];
    self.detailLable.alpha = 0.8;
    
    
    self.newsImageView.layer.masksToBounds = YES;
    self.newsImageView.layer.cornerRadius = 20 * [UIScreen mainScreen].bounds.size.width/320;
    

}





- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
