//
//  SecondNewsTableViewCell.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "SecondNewsTableViewCell.h"
#import "UIImageView+WebCache.h"
@interface SecondNewsTableViewCell()
@property (strong, nonatomic) IBOutlet UILabel *titleLable;

@property (strong, nonatomic) IBOutlet UIImageView *oneNewsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *twoNewsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *threeNewsImageView;

//@property (strong, nonatomic)  UIImageView *oneNewsImageView;
//@property (strong, nonatomic)  UIImageView *twoNewsImageView;
//@property (strong, nonatomic)  UIImageView *threeNewsImageView;
@end

@implementation SecondNewsTableViewCell
#define WAWA_FONT @"DFWaWaW5-GB"

#define POP_FONT @"DFPOP1W5-GB"

#define YUAN_FONT @"DFYuanW5-GB"

#define LIBIAN_FONT @"Libian SC"

#define WEBEI_FONT @"Weibei-SC-Bold"

#define TITLE_FONT @"Libian SC"
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
    }
    return self;
}

-(void)setModel:(NewsModels *)model
{
    self.titleLable.text = model.title;

    [self.oneNewsImageView sd_setImageWithURL:[NSURL URLWithString:model.imgextra1]];
    [self.twoNewsImageView sd_setImageWithURL:[NSURL URLWithString:model.imgextra2]];
    [self.threeNewsImageView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc]];
    

    #pragma mark--- 具体细节
    
    self.oneNewsImageView.layer.masksToBounds = YES;
    self.oneNewsImageView.layer.cornerRadius = 15 * [UIScreen mainScreen].bounds.size.width/320;
    
    self.twoNewsImageView.layer.masksToBounds = YES;
    self.twoNewsImageView.layer.cornerRadius = 15 * [UIScreen mainScreen].bounds.size.width/320;
    
    self.threeNewsImageView.layer.masksToBounds = YES;
    self.threeNewsImageView.layer.cornerRadius = 15 * [UIScreen mainScreen].bounds.size.width/320;
    


}



//
- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
