//
//  RadioPlayTableViewCell.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/17.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "RadioPlayTableViewCell.h"


@interface RadioPlayTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@property (weak, nonatomic) IBOutlet UILabel *timeLable;


@end


@implementation RadioPlayTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    

    }
    return self;
}


-(void)setModel:(RadioPlayModel *)model
{
    self.titleLable.text = model.title;
    self.timeLable.text = model.lmodify;
    
}




- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
