//
//  SecondNewsTableViewCell.h
//  LWB_Project
//
//  Created by lanou3g on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"NewsModels.h"
@interface SecondNewsTableViewCell : UITableViewCell

@property (nonatomic, strong) NewsModels *model;

@end
