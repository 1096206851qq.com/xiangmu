//
//  RadioDetailTableViewCell.h
//  LWB_Project
//
//  Created by lanou3g on 15/11/17.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioDetailModel.h"
@interface RadioDetailTableViewCell : UITableViewCell

//-(void)setRadioDetailMode:(RadioDetailModel *)detailModel;
@property (nonatomic, strong) RadioDetailModel *Model;
@end
