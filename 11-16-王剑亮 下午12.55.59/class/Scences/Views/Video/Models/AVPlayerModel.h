//
//  AVPlayerModel.h
//  
//
//  Created by 刘瑞龙 on 15/9/10.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AVPlayerModel : NSObject

@property (nonatomic, assign) CGSize videoSize;

@property (nonatomic, copy) NSString * videoUrlStr;

@property (nonatomic, copy) NSString * videoTitle;


@end
