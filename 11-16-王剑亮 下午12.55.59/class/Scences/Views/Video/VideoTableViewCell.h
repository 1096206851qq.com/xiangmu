//
//  VideoTableViewCell.h
//  LWB_Project
//
//  Created by lanou3g on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"
#import "LRLAVPlayerView.h"

@interface VideoTableViewCell : UITableViewCell

@property (nonatomic, strong) VideoModel *model;

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) UIView *superView;


-(void)switchVideoWindow:(NSInteger)index;
@property (nonatomic, copy) void (^blockIndex)(NSInteger selectIndex);
+(BOOL)GetplayerStates;
@end
