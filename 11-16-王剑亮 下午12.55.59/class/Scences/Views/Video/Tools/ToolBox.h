//
//  ToolBox.h
//  LRLAVPalyer
//
//  Created by 孙玮超 on 15/11/6.
//
#import <Foundation/Foundation.h>

@interface ToolBox : NSObject

+(NSString *)calculateTimeWithTimeFormatter:(long long)timeSecond;

@end
