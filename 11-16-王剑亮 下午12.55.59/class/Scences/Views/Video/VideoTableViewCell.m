//
//  VideoTableViewCell.m
//  LWB_Project
//
//  Created by lanou3g on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "VideoTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "AVPlayerModel.h"

static NSInteger CurrentIndex = -1;
@interface VideoTableViewCell ()<LRLAVPlayDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *myimage;
@property (strong, nonatomic) IBOutlet UILabel *titleLable;

@property (strong, nonatomic) IBOutlet UILabel *desLable;

@property (strong, nonatomic) IBOutlet UIButton *startButton;


//用来模拟视频的model
@property (nonatomic, strong) AVPlayerModel * avModel;



@end

@implementation VideoTableViewCell

////用来播放视频的view
//@property (nonatomic, strong) LRLAVPlayerView * avplayerView;
//这样 这个就是一个对象了

static LRLAVPlayerView * avplayerView;

+(BOOL)GetplayerStates
{
    return avplayerView.isPlaying;
}

- (void)awakeFromNib
{


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];


}

- (IBAction)startPlay:(UIButton *)sender {

    NSString *NetStates1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"NETStates"];
    if ([NetStates1 isEqualToString:@"YES"])
    {
        //移除
        if ([avplayerView superview] != nil)
        {
            //上一个cell的图片要显示出来
            for (UIView *view in avplayerView.superview.subviews) {
                if (view.alpha == 0) {
                    view.alpha = 1;
                }
            }
            [avplayerView destoryAVPlayer];
            [avplayerView removeFromSuperview];
            avplayerView = nil;
        }
        
        
        self.avModel.videoUrlStr = _model.mp4_url;
        self.avModel.videoSize = CGSizeMake(SCREEN_WIDTH-20,self.myimage.bounds.size.height);
        [self createAVPlayer];
        
        #pragma mark--- 必须要有！
        CurrentIndex =_index;
        #pragma mark--- 回传到上个界面记录一下
        if (_blockIndex)
        {
            _blockIndex(CurrentIndex);
        }

    
    }
    else
    {
        //提示没网！
        [self checkNetalerView];
    }
    
 
    
}
-(void)checkNetalerView
{

    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请检查网络连接" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
    [alertView show];
    

}

-(void)setModel:(VideoModel *)model
{
    _model = model;
    
    [self.myimage sd_setImageWithURL:[NSURL URLWithString:model.cover]];
    self.titleLable.text = model.title;
    self.desLable.text = [NSString stringWithFormat:@"%@",model.description1];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(CurrentIndex == _index)
    {
      [self switchVideoWindow:0];
    }else
    {
     [self.contentView bringSubviewToFront:  self.myimage];
     [self.contentView bringSubviewToFront:  self.startButton];
     
        UIView *view = [self viewWithTag:1000];
        if (view)
        if(!avplayerView.isPlaying)
        {
            //这里边如果有视频播放窗口要移除 要不然停止的时候 会有Cell重用的现象
            [avplayerView removeFromSuperview];
        }

    }
}

-(void)switchVideoWindow:(NSInteger)index
{

    if (index)
    {
        //播放到的时候才换
        if(avplayerView.isPlaying)
        {
            [avplayerView removeFromSuperview];
            self.avModel.videoSize = CGSizeMake(_superView.bounds.size.width,_superView.bounds.size.width);
            [self.superView addSubview:avplayerView];
            avplayerView.backgroundColor = [UIColor blackColor];
            
            [_superView bringSubviewToFront:avplayerView];
            __weak VideoTableViewCell * weakSelf = self;

                
            [avplayerView setPositionWithPortraitBlock:^(MASConstraintMaker *make) {  //竖屏
                make.top.equalTo(weakSelf.superView).with.offset(0);
                make.left.equalTo(weakSelf.superView).offset(0);
                make.width.equalTo(weakSelf.superView);
                make.height.equalTo(weakSelf.superView);
            } andLandscapeBlock:^(MASConstraintMaker *make) {
                make.width.equalTo(@(SCREEN_HEIGHT));
                make.height.equalTo(@(SCREEN_WIDTH));
                make.center.equalTo(Window);
            }];

            //自己记录一下自己的tag
            avplayerView.tag = 1000;
            #pragma mark--- 停止的时候回调block
            avplayerView.blockstop = ^(void)
            {
                [avplayerView destoryAVPlayer];
                [avplayerView removeFromSuperview];
                avplayerView = nil;
            };
        }
    }
    else
    {
        [avplayerView removeFromSuperview];
        [self.contentView addSubview: avplayerView];
        self.avModel.videoSize = CGSizeMake(SCREEN_WIDTH-20,self.myimage.bounds.size.height);
    
        __weak VideoTableViewCell * weakSelf = self;
        [avplayerView setPositionWithPortraitBlock:^(MASConstraintMaker *make) {  //竖屏
            make.top.equalTo(weakSelf.contentView).with.offset(0);
            make.left.equalTo(weakSelf.contentView).offset(10);
            make.right.equalTo(weakSelf.contentView).offset(-10);
            make.bottom.equalTo(weakSelf.contentView).with.offset(-60);  //视频初始高度必须这么写
        } andLandscapeBlock:^(MASConstraintMaker *make) {
            make.width.equalTo(@(SCREEN_HEIGHT));
            make.height.equalTo(@(SCREEN_WIDTH));
            make.center.equalTo(Window);
        }];
        //自己记录一下自己的tag
        avplayerView.tag = 1000;
        #pragma mark--- 停止的时候回调block
        avplayerView.blockstop = ^(void)
        {
            self.myimage.alpha = 1;
            [avplayerView destoryAVPlayer];
            [avplayerView removeFromSuperview];
            avplayerView = nil;
        };
        
    }

}



#pragma mark - 创建用于播放的View
-(void)createAVPlayer{


        avplayerView = [LRLAVPlayerView avplayerViewWithVideoUrlStr:self.model.mp4_url andInitialHeight:self.myimage.bounds.size.height andSuperView:self.contentView];
        avplayerView.delegate = self;
        [self.contentView addSubview:avplayerView];
        avplayerView.backgroundColor = [UIColor blackColor];

        __weak VideoTableViewCell * weakSelf = self;
        [avplayerView setPositionWithPortraitBlock:^(MASConstraintMaker *make)
         {  //竖屏
        make.top.equalTo(weakSelf.contentView).with.offset(0);
        make.left.equalTo(weakSelf.contentView).offset(10);
        make.right.equalTo(weakSelf.contentView).offset(-10);
        make.bottom.equalTo(weakSelf.contentView).with.offset(-60);  //视频初始高度必须这么写
            
        } andLandscapeBlock:^(MASConstraintMaker *make) {
            make.width.equalTo(@(SCREEN_HEIGHT));
            make.height.equalTo(@(SCREEN_WIDTH));
            make.center.equalTo(Window);
        }];
        
    //自己记录一下自己的tag
    avplayerView.tag = 1000;
    #pragma mark--- 停止的时候回调block
    avplayerView.blockstop = ^(void)
    {
        [avplayerView destoryAVPlayer];
        [avplayerView removeFromSuperview];
        avplayerView = nil;
    };
    
    
    [self switchVideoWindow:0];
    

}

#pragma mark - 下面是懒加载
-(AVPlayerModel *)avModel{
    if (!_avModel) {
        _avModel = [[AVPlayerModel alloc] init];
        _avModel.videoSize = CGSizeMake(SCREEN_WIDTH-20,self.myimage.bounds.size.height);
        //_avModel.videoTitle = @"那些年";
        _avModel.videoUrlStr = _model.mp4_url;
    }
    return _avModel;
}

#pragma mark - 关闭设备自动旋转, 然后手动监测设备旋转方向来旋转avplayerView
-(BOOL)shouldAutorotate{
    return NO;
}




@end
