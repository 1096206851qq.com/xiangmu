//
//  NewsDetailImgModel.m
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/13.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "NewsDetailImgModel.h"

@implementation NewsDetailImgModel
/** 便利构造器方法 */
+ (instancetype)detailImgWithDict:(NSDictionary *)dict
{
    NewsDetailImgModel *imgModel = [[self alloc] init];
    imgModel.ref = dict[@"ref"];
    imgModel.pixel = dict[@"pixel"];
    imgModel.src = dict[@"src"];
    
    return imgModel;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}
@end
