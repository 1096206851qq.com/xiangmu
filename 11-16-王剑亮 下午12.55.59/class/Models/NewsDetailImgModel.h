//
//  NewsDetailImgModel.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/13.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsDetailImgModel : NSObject
@property (nonatomic, copy) NSString *src;
/** 图片尺寸 */
@property (nonatomic, copy) NSString *pixel;
/** 图片所处的位置 */
@property (nonatomic, copy) NSString *ref;

@property(nonatomic,copy)NSString *source_url;

+ (instancetype)detailImgWithDict:(NSDictionary *)dict;
@end
