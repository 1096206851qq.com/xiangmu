//
//  NewsModels.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsModels : NSObject

#pragma mark--- 这里边有两种类型的数据 对应自定义的两种cell
#pragma mark--- 大家都有的共性的

#pragma mark--- 标题
@property (nonatomic, copy) NSString *title;
#pragma mark--- 详情
@property (nonatomic, copy) NSString *digest;

#pragma mark--- 展示的图片网址
@property (nonatomic, copy) NSString *imgsrc;
#pragma mark--- 信息的来源
@property (nonatomic, copy) NSString *source;
#pragma mark--- 到时候拼接用可能
@property (nonatomic, copy) NSString *docid;
#pragma mark--- 修正的时间
@property (nonatomic, copy) NSString *lmodify;
#pragma mark--- 可能是开始时间 没用目前
@property (nonatomic, copy) NSString *ptime;
#pragma mark--- 通过这个判断模型是那个类型的 "photoset"是图片类型的
@property (nonatomic, copy) NSString *skipType;


#pragma mark--- 这个是图片类型独特具有的属性
#pragma mark--- 前两个图片网址
@property (nonatomic, copy) NSString *imgextra1;
@property (nonatomic, copy) NSString *imgextra2;
#pragma mark--- 显示的图片网址拼接 所需要的图片地址 是在下个界面需要的信息
@property (nonatomic, copy) NSString *photosetID;


#pragma mark--- 这个是普通的celll类型 对应的独特属性
#pragma mark--- 两个网址 内容一致 默认用第一个
@property (nonatomic, copy) NSString *url_3w;
@property (nonatomic, copy) NSString *url;


#pragma mark--- 排除解决不了的新闻类型的判断标志
@property (nonatomic, copy) NSString *editorBit;
@property (nonatomic, copy) NSString *TAGS;




@end
