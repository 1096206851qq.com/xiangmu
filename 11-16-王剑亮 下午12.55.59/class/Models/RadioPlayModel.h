//
//  RadioPlayModel.h
//  LWB_Project
//
//  Created by lanou3g on 15/11/17.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RadioPlayModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *lmodify;
@property (nonatomic, strong) NSString *docid;
@property (nonatomic, strong) NSString *imgurl;

@end
