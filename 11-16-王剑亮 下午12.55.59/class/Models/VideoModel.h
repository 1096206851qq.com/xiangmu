//
//  VideoModel.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/16.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoModel : NSObject
@property (nonatomic,copy) NSString * title;       //标题，描述
@property (nonatomic,copy) NSString * cover;       //封面图片URL
@property (nonatomic,copy) NSString * mp4_url;     //MP4视频URL
@property (nonatomic,copy) NSString * replyCount;  //跟帖人数
@property (nonatomic,copy) NSString * playCount;   //播放次数
@property (nonatomic,copy) NSString * length;      //时长
@property (nonatomic,copy) NSString * ptime;       //更新时间
@property (nonatomic,copy) NSString * replyid;//跟帖
@property (nonatomic,copy) NSString * description1; //描述


@end
