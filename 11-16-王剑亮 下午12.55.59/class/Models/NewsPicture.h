//
//  NewsPicture.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/13.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsPicture : NSObject
@property (nonatomic, copy) NSString *imageUrl;
@end
