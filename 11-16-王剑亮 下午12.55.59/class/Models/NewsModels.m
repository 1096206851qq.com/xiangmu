//
//  NewsModels.m
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/12.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import "NewsModels.h"
#import "DebugNSLog.h"
@implementation NewsModels

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
//    MyLog(@"value == %@",value);
//    MyLog(@"key == %@",key);
    
    
    if ([key isEqualToString:@"imgextra"]) {
        
        NSDictionary *dic1 = value[0];
        NSDictionary *dic2 = value[1];
        
        MyLog(@"dic1 ===== %@",[dic1 allValues][0]);
        MyLog(@"dic2 ===== %@",[dic2 allValues][0]);
        
        [self setValue:[dic1 allValues][0] forKey:@"imgextra1" ];
        [self setValue:[dic2 allValues][0] forKey:@"imgextra2" ];
 
    }
    
    //排除解析不了的新闻信息
    if ([key isEqualToString:@"editor"]) {
        

        [self setValue:@"YES" forKey:@"editorBit" ];
    
        
    }
    
    
}

@end
