//
//  RadioModel.h
//  LWB_Project
//
//  Created by lanou3g on 15/11/15.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RadioModel : NSObject

@property (nonatomic, copy) NSString *cid;//传到下一个界面的id
@property (nonatomic, copy) NSString *cname;//分区标题
@property (nonatomic, copy) NSString *tname;//电台标题
@property (nonatomic, copy) NSString *docid;//传下个界面
@property (nonatomic, copy) NSString *title;//标题描述
@property (nonatomic, copy) NSString *imgsrc;//图片网址
@property (nonatomic, copy) NSString *playCount;//播放次数
@property (nonatomic, copy) NSString *tid;//播放界面用


@end
