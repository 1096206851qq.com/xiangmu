//
//  FMPlayingModel.h
//  LWB_Project
//
//  Created by 王剑亮 on 15/11/19.
//  Copyright © 2015年 wangjianliang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FMPlayingModel : NSObject

@property (nonatomic,strong) NSString * url_mp4; //音乐网址

@end
